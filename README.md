# README #

This project is created for CS.

IMarketMaker interface defines expected functionality of the "Live Order Board" implementation. 

```java
    long register(String userId, OrderType orderType, double price, double quantity );
    long register(Order order);
    boolean cancel(long orderId) throws OrderNotFoundException;
    Map<OrderType, List<OrderSummary>> summary();
    String prettySummary();
```


Implementation of this interface can be found in MarketMaker.

* Implementation is written in thread-safe manner directly instead of wrapping non-thread safe version.
* Data mutating methods are protected with WriteLock and summary is protected with ReadLock.
* MarketMaker.summary implementation is simplified by tracking updates on OrderSummary after each order registration/cancellation.
* MarketMaker.prettySummary is added for convenience.  

**MarketMakerTest** check correctness of the behaviours above in single-threaded environment.
**ConcurrentMarketMakerTest** confirms correctness in multi-threaded environment. Explanation of how this is done is explained in test.
 
 