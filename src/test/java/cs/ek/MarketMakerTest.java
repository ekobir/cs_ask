package cs.ek;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

public class MarketMakerTest
{
    private MarketMaker marketMaker;

    @Test
    public void testOrderProcessing() throws OrderNotFoundException
    {
        List<Long> orderIds = new ArrayList<>(4);
        orderIds.add(marketMaker.register(Order.of("A", OrderType.BUY, 100, 1)));
        orderIds.add(marketMaker.register("B", OrderType.BUY, 99, 1));

        orderIds.add(marketMaker.register("C", OrderType.SELL, 99, 1));
        orderIds.add(marketMaker.register(Order.of("D", OrderType.SELL, 101, 1)));

        Assert.assertEquals(orderIds, marketMaker.orderIds());

        marketMaker.cancel(2L);
        marketMaker.cancel(3L);

        Assert.assertEquals(createCollection(1,4), marketMaker.orderIds());
        orderIds.add(marketMaker.register(Order.of("E", OrderType.SELL, 103, 1)));
        Assert.assertEquals(createCollection(1,4,5), marketMaker.orderIds());
    }

    @Test(expected = OrderNotFoundException.class)
    public void testCancellationOfNotAddedOrder() throws OrderNotFoundException
    {
        marketMaker.cancel(1L);
    }

    @Test(expected = OrderNotFoundException.class)
    public void testMultipleCancel() throws OrderNotFoundException
    {
        marketMaker.register(Order.of("A", OrderType.BUY, 100, 1));
        Assert.assertEquals(createCollection(1), marketMaker.orderIds());

        marketMaker.cancel(1L);
        Assert.assertEquals(Collections.emptyList(), marketMaker.orderIds());
        marketMaker.cancel(1L);
    }


    Collection<Long> createCollection(int ...ids)
    {
        return Arrays.stream(ids).asLongStream().boxed().collect(Collectors.toList());
    }

    @Test
    public void testOrderSummary() throws OrderNotFoundException
    {
        Map<OrderType, List<OrderSummary>> empty = new HashMap<>();
        empty.put(OrderType.SELL, Collections.emptyList());
        empty.put(OrderType.BUY, Collections.emptyList());

        Assert.assertEquals(empty, marketMaker.summary());

        marketMaker.register(Order.of("A", OrderType.BUY, 100, 1));
        marketMaker.register("B", OrderType.BUY, 99, 1);

        marketMaker.register("C", OrderType.SELL, 99, 1);
        marketMaker.register(Order.of("D", OrderType.SELL, 101, 1));

        Map<OrderType, List<OrderSummary>> summary = marketMaker.summary();
        Assert.assertEquals(
                Arrays.asList(
                        new OrderSummary(OrderType.BUY, 100, 1),
                        new OrderSummary(OrderType.BUY, 99, 1)
                ), summary.get(OrderType.BUY));

        Assert.assertEquals(
                Arrays.asList(
                        new OrderSummary(OrderType.SELL, 99, 1),
                        new OrderSummary(OrderType.SELL, 101, 1)
                ), summary.get(OrderType.SELL));

        //System.out.println(marketMaker.prettySummary());

        marketMaker.cancel(4L);
        marketMaker.register("C", OrderType.BUY, 100, 1);
        marketMaker.cancel(2L);

        marketMaker.register("D", OrderType.SELL, 99, 2);
        marketMaker.register("A", OrderType.SELL, 100, 3);

        summary = marketMaker.summary();
        Assert.assertEquals(
                Arrays.asList(
                        new OrderSummary(OrderType.BUY, 100, 2)
                ), summary.get(OrderType.BUY));

        Assert.assertEquals(
                Arrays.asList(
                        new OrderSummary(OrderType.SELL, 99, 3),
                        new OrderSummary(OrderType.SELL, 100, 3)
                ), summary.get(OrderType.SELL));

        // Cancelling all Buys
        marketMaker.cancel(5L);
        marketMaker.cancel(1L);
        summary = marketMaker.summary();
        Assert.assertEquals(Collections.<OrderSummary>emptyList(), summary.get(OrderType.BUY));

        Assert.assertEquals(
                Arrays.asList(
                        new OrderSummary(OrderType.SELL, 99, 3),
                        new OrderSummary(OrderType.SELL, 100, 3)
                ), summary.get(OrderType.SELL));
    }


    @Before
    public void setUp()
    {
        marketMaker = new MarketMaker();
    }
}
