package cs.ek;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class ConcurrentMarketMakerTest
{
    /*
       The idea is to register&cancel orders in multithreaded fashion and at the end to
       check final state by calling MarketMaker.summary()

       After a thread registers an order, it might add cancellation task for the same order to the queue
       with 0.6 probability
     */
    @Test
    public void testConcurrentAccess() throws Exception
    {
        final BlockingQueue<Runnable> taskQueue = new LinkedBlockingQueue<>();
        final BlockingQueue<Order> cancelledOrders = new LinkedBlockingQueue<>();
        int nThreads = Runtime.getRuntime().availableProcessors();
        final ThreadPoolExecutor tpe = new ThreadPoolExecutor(nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                taskQueue);

        IMarketMaker marketMaker = new MarketMaker();

        List<Order> orders = Arrays.asList(
                Order.of("A", OrderType.BUY, 100, 5),
                Order.of("B", OrderType.BUY, 101, 5),
                Order.of("A", OrderType.SELL, 120, 4),
                Order.of("C", OrderType.SELL, 110, 7),
                Order.of("D", OrderType.BUY, 100, 2),
                Order.of("E", OrderType.BUY, 102, 5),
                Order.of("B", OrderType.SELL, 105, 1),
                Order.of("B", OrderType.SELL, 100, 9),
                Order.of("C", OrderType.SELL, 110, 1),
                Order.of("D", OrderType.BUY, 99, 2),
                Order.of("E", OrderType.SELL, 120, 5),
                Order.of("D", OrderType.BUY, 105, 7),
                Order.of("C", OrderType.BUY, 101, 6),
                Order.of("A", OrderType.SELL, 104, 8),
                Order.of("A", OrderType.BUY, 105, 3),
                Order.of("C", OrderType.SELL, 100, 2)
        );

        orders.stream().map(o -> new RegistrationTask(taskQueue, cancelledOrders, marketMaker, o))
                .forEach(tpe::submit);

        Thread.sleep(500);
        tpe.shutdown();
        tpe.awaitTermination(500, TimeUnit.MILLISECONDS);

        // Not certainty but cancelled orders should not be empty!!!
        Assert.assertFalse(cancelledOrders.isEmpty());

        // Another MarketMaker instance which will be used in single threaded
        IMarketMaker controller = new MarketMaker();
        //Find and remove all the cancelled Orders
        Set<Wrapper> wrappedOrders = orders.stream().map(Wrapper::new).collect(Collectors.toSet());
        Set<Wrapper> wrappedCancellation = cancelledOrders.stream().map(Wrapper::new).collect(Collectors.toSet());
        wrappedOrders.removeAll(wrappedCancellation);

        // Register all non-cancelled orders to Market Maker controller
        wrappedOrders.stream().map(Wrapper::getOrder).forEach(controller::register);

        // Finally compare
        Assert.assertEquals(controller.summary(), marketMaker.summary());
    }



    private static class RegistrationTask implements Runnable
    {
        private final BlockingQueue<Runnable> taskQueue;
        private final BlockingQueue<Order> cancelledOrders;
        private final Order order;
        private final IMarketMaker marketMaker;

        public RegistrationTask(BlockingQueue<Runnable> taskQueue, BlockingQueue<Order> cancelledOrders,
                                IMarketMaker marketMaker, Order order) {
            this.taskQueue = taskQueue;
            this.cancelledOrders = cancelledOrders;
            this.marketMaker = marketMaker;
            this.order = order;
        }

        @Override
        public void run(){
            final long orderId = marketMaker.register(order);
            final float prob = ThreadLocalRandom.current().nextFloat();
            if(prob >= 0.4)
            {
                cancelledOrders.add(order);
                taskQueue.add(new CancellationTask(marketMaker, orderId));
            }
        }
    }

    private static class CancellationTask implements Runnable
    {
        private final IMarketMaker marketMaker;
        private final long orderId;

        public CancellationTask(IMarketMaker marketMaker, long orderId) {
            this.marketMaker = marketMaker;
            this.orderId = orderId;
        }

        @Override
        public void run(){
            try {
                marketMaker.cancel(orderId);
            }catch (Exception e){
                throw new RuntimeException(e);
            }
        }
    }

    private static class Wrapper{
        private final Order order;

        public Wrapper(Order order) {
            this.order = order;
        }

        public Order getOrder() {
            return order;
        }

        @Override
        public boolean equals(Object w) {
            if (this == w) return true;
            if (w == null || getClass() != w.getClass()) return false;
            Order o = ((Wrapper) w).order;

            return Double.compare(order.getQuantity(), o.getQuantity()) == 0 &&
                    Double.compare(order.getPrice(), o.getPrice()) == 0 &&
                    Objects.equals(o.getUserId(), order.getUserId()) &&
                    o.getOrderType() == order.getOrderType();
        }

        @Override
        public int hashCode() {
            return Objects.hash(order.getUserId(), order.getQuantity(), order.getPrice(), order.getOrderType());
        }
    }
}
