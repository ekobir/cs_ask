package cs.ek;

import java.util.Objects;

import static org.apache.commons.lang3.Validate.*;

public class Order
{
    private final String userId;
    private final double quantity;
    private final double price;
    private final OrderType orderType;

    private Order(String userId, double quantity, double price, OrderType orderType) {
        this.userId = userId;
        this.quantity = quantity;
        this.price = price;
        this.orderType = orderType;
    }

    public static Order of(String userId, OrderType orderType, double price, double quantity )
    {
        notBlank(userId, "User id must be provided.");
        isTrue(quantity > 0, " Order quantity should be greater than 0");
        isTrue(price >= 0, "Order price can not be negative.");
        notNull(orderType);

        return new Order(userId, quantity, price, orderType);
    }

    public String getUserId() {
        return userId;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public OrderType getOrderType() {
        return orderType;
    }
}
