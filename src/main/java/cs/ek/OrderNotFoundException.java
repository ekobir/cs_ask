package cs.ek;

public class OrderNotFoundException extends Exception
{
    public OrderNotFoundException(long orderId){
        super("Order with id:"+ orderId + " not found.");
    }
}
