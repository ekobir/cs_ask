package cs.ek;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Objects;

public class OrderSummary
{
    private final double price;
    private final OrderType orderType;
    private double quantity;

    public OrderSummary(OrderType orderType, double price, double quantity ) {
        this.orderType = orderType;
        this.price = price;
        this.quantity = quantity;
    }

    OrderSummary(Pair<OrderType, Double> summaryKey)
    {
        this(summaryKey.getLeft(), summaryKey.getRight(), 0);
    }

    void incBy(Order order)
    {
       this.quantity += order.getQuantity();
    }

    void reduceBy(Order order)
    {
        this.quantity -= order.getQuantity();
    }

    public double getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderSummary summary = (OrderSummary) o;
        return Double.compare(summary.price, price) == 0 &&
                Double.compare(summary.quantity, quantity) == 0 &&
                orderType == summary.orderType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, orderType, quantity);
    }

    @Override
    public String toString() {
        return orderType.name() +" - " + quantity + " kg for £" + price;
    }

    public String toStringNoType() {
        return quantity + " kg for £" + price;
    }

}
