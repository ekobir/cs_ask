package cs.ek;

import java.util.List;
import java.util.Map;

public interface IMarketMaker
{
    long register(String userId, OrderType orderType, double price, double quantity );
    long register(Order order);
    boolean cancel(long orderId) throws OrderNotFoundException;
    Map<OrderType, List<OrderSummary>> summary();
    String prettySummary();

}
