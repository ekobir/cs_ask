package cs.ek;

import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import static org.apache.commons.lang3.StringUtils.*;

public class MarketMaker implements IMarketMaker
{
    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock.ReadLock rl = rwl.readLock();
    private final ReentrantReadWriteLock.WriteLock wl = rwl.writeLock();

    private long nextOrderIdGen = 0;

    private Map<Long, Order> orderMap = new LinkedHashMap<>();
    private Map<Pair<OrderType, Double>, OrderSummary> summaryMap = new TreeMap<>((o1, o2) -> {
        int mul1 = o1.getKey() == OrderType.BUY ? -1 : 1;
        int mul2 = o2.getKey() == OrderType.BUY ? -1 : 1;
        return Double.compare(mul1 * o1.getRight(), mul2 * o2.getRight());
    });

    @Override
    public long register(String userId, OrderType orderType, double price, double quantity ) {
        return register(Order.of(userId, orderType, price, quantity));
    }

    @Override
    public long register(Order order) {
        wl.lock();
        try {
            final long orderId = ++nextOrderIdGen;
            orderMap.put(orderId, order);
            addToSummary(order);
            return orderId;
        } finally {
            wl.unlock();
        }
    }


    @Override
    public boolean cancel(long orderId) throws OrderNotFoundException {
        wl.lock();
        try {
            Order order = orderMap.remove(orderId);
            if(order == null)
                throw new OrderNotFoundException(orderId);

            rmFromSummary(order);
            return true;
        } finally {
            wl.unlock();
        }
    }

  Collection<Order> orders()
  {
        rl.lock();
        try
        {
            return new ArrayList<>(orderMap.values());
        } finally {
            rl.unlock();
        }
    }

    Collection<Long> orderIds()
    {
        rl.lock();
        try
        {
            return new ArrayList<>(orderMap.keySet());
        } finally {
            rl.unlock();
        }
    }


    @Override
    public Map<OrderType, List<OrderSummary>> summary() {
        rl.lock();
        try
        {
            Map<OrderType, List<OrderSummary>> summary = summaryMap.values().stream()
                    .collect(Collectors.groupingBy(OrderSummary::getOrderType));

            // Instead of returning null, making sure that empty list returned.
            for(OrderType orderType : OrderType.values())
                summary.putIfAbsent(orderType, Collections.emptyList());

            return summary;
        } finally {
            rl.unlock();
        }
    }

    @Override
    public String prettySummary() {
        Map<OrderType, List<OrderSummary>> data = summary();
        final List<OrderSummary> buys = data.getOrDefault(OrderType.BUY, Collections.emptyList());
        final List<OrderSummary> sells = data.getOrDefault(OrderType.SELL, Collections.emptyList());

        final List<String> buysStr = buys.stream().map(OrderSummary::toStringNoType).collect(Collectors.toList());
        final List<String> sellStr = sells.stream().map(OrderSummary::toStringNoType).collect(Collectors.toList());
        int l = Math.min(buysStr.size(), sellStr.size());

        String filled = repeat("*", 34);
        StringBuilder sb = new StringBuilder(250);
        sb.append("***************SELL***************|***************BUY****************\n");

        for(int i = 0; i< l; i++){
            sb.append(rightPad(sellStr.get(i), 34)).append('|').append(rightPad(buysStr.get(i), 34)).append("\n");
        }
        for(int i = l; i < sellStr.size(); i++){
            sb.append(rightPad(sellStr.get(i), 34)).append('|').append(filled).append("\n");
        }

        for(int i = l; i < sellStr.size(); i++){
            sb.append(filled).append('|').append(rightPad(buysStr.get(i), 34)).append("\n");
        }
        sb.append("*******************************************************************");



        return sb.toString();
    }



    private void addToSummary(Order order)
    {
        Pair<OrderType, Double> summaryKey = Pair.of(order.getOrderType(), order.getPrice());
        OrderSummary summary = summaryMap.get(summaryKey);
        if(summary != null){
            summary.incBy(order);
        } else{
            summary = new OrderSummary(order.getOrderType(), order.getPrice(), order.getQuantity());
        }
        summaryMap.put(summaryKey, summary);
    }

    private void rmFromSummary(Order order)
    {
        Pair<OrderType, Double> summaryKey = Pair.of(order.getOrderType(), order.getPrice());
        OrderSummary summary = summaryMap.get(summaryKey);
        summary.reduceBy(order);
        if(summary.getQuantity() == 0)
            summaryMap.remove(summaryKey);
        else
            summaryMap.put(summaryKey, summary);
    }
}
